package com.example.database2excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
public class Database2excelApplication implements CommandLineRunner {
    final private Environment env;

    public Database2excelApplication(Environment env) {
        this.env = env;
    }

    public static void main(String[] args) {
        SpringApplication.run(Database2excelApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // Export database table
        export(args);
    }

    public void export(String... args) {
        String jdbcURL = env.getProperty("SPRING_DATASOURCE_URL", "");
        String username = env.getProperty("SPRING_DATASOURCE_USERNAME");
        String password = env.getProperty("SPRING_DATASOURCE_PASSWORD");
        String excelFilePath = env.getProperty("FILENAME", "");

        String[] tables = args;
        if (excelFilePath.trim().isEmpty()) {
            excelFilePath = getFileName(args[0]);
            tables = Arrays.copyOfRange(args, 1, args.length);
        }

        try (Connection connection = DriverManager.getConnection(jdbcURL, username, password)) {
            SXSSFWorkbook workbook = new SXSSFWorkbook();
            Statement statement = connection.createStatement();

            for (String table : tables) {
                String[] tableName = table.split("\\.");
                Sheet sheet = workbook.createSheet(tableName[tableName.length - 1]);
                String sql = "SELECT * FROM ".concat(table);
                ResultSet result = statement.executeQuery(sql);
                writeHeaderLine(result, sheet);
                writeDataLines(result, workbook, sheet);
            }

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();

            statement.close();

        } catch (SQLException e) {
            System.out.println("Database error:");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File IO error:");
            e.printStackTrace();
        }
    }

    private String getFileName(String baseName) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String dateTimeInfo = dateFormat.format(new Date());
        return baseName.concat(String.format("_%s.xlsx", dateTimeInfo));
    }

    private void writeHeaderLine(ResultSet result, Sheet sheet) throws SQLException {
        // write header line containing column names
        ResultSetMetaData metaData = result.getMetaData();
        int numberOfColumns = metaData.getColumnCount();

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < numberOfColumns; i++) {
            String columnName = metaData.getColumnName(i + 1);
            Cell headerCell = headerRow.createCell(i);
            headerCell.setCellValue(columnName);
        }
    }

    private void writeDataLines(ResultSet result, Workbook workbook, Sheet sheet) throws SQLException {
        ResultSetMetaData metaData = result.getMetaData();
        int numberOfColumns = metaData.getColumnCount();

        AtomicInteger rowIndex = new AtomicInteger(1);
        while (result.next()) {
            Row row = sheet.createRow(rowIndex.getAndIncrement());

            for (int i = 0; i < numberOfColumns; i++) {
                Object valueObject = result.getObject(i + 1);
                Cell cell = row.createCell(i);

                if (valueObject == null) {
                    cell.setCellValue("(null)");
                }
                else if (valueObject instanceof Boolean) {
                    cell.setCellValue((Boolean) valueObject);
                }
                else if (valueObject instanceof Double) {
                    cell.setCellValue((double) valueObject);
                }
                else if (valueObject instanceof Float) {
                    cell.setCellValue((float) valueObject);
                }
                else if (valueObject instanceof Timestamp) {
                    cell.setCellValue((Timestamp) valueObject);
                    formatDateCell(workbook, cell);
                }
                else {
                    cell.setCellValue(String.format("%s", valueObject));
                }
            }
        }
    }

    private void formatDateCell(Workbook workbook, Cell cell) {
        CellStyle cellStyle = workbook.createCellStyle();
        CreationHelper creationHelper = workbook.getCreationHelper();
        cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
        cell.setCellStyle(cellStyle);
    }

}
